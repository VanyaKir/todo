from django.test import TestCase
from .models import Todo


class TodoModelsTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Todo.objects.create(title='first todo', body='a body todo')

    def test_title_content(self):
        todo = Todo.objects.get(pk=1)
        expected_object_name = f'{todo.title}'
        self.assertEqual(expected_object_name, 'first todo')

    def test_body_content(self):
        todo = Todo.objects.get(pk=1)
        expected_object_name = f'{todo.body}'
        self.assertEqual(expected_object_name, 'a body todo')